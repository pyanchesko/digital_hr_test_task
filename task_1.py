# !/usr/bin/env python3
"""Сложность - O(n)
   Память - O(n)
   Субъективная сложность - 1/10
   Времени потратил - 1 час
   разумный размер входных параметров - 1000000
"""
from typing import List


def get_p(parts: List[float]) -> List[float]:
    total = sum(parts)
    return [i / total for i in parts]


if __name__ == '__main__':
    parts = []
    line_count = int(input())
    for i in range(line_count):
        line = input()
        parts.append(float(line))

    result = get_p(parts)
    for i in result:
        print(f'{i:.3f}')
    print()
