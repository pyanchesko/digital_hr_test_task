# !/usr/bin/env python3
"""Сложность - O(nlog(n))
   Память - O(n)
   Субъективная сложность - 6/10
   Времени потратил - 8 часов(сначала шел не тем путем, другим алгоритмом. Финальное решение написал за час)
   разумный размер входных параметров - 1000000
"""

from typing import List


class Bond:
    def __init__(self, name, weight, price, total_days, pay_day):
        self.name = name
        self.pay_day = pay_day
        self.raw_price = price
        self.raw_weight = weight
        self.weight = weight * 10 * price
        self.price = price * (30 + total_days - self.pay_day) + 1000 * price
        self.profit = self.get_profit()

    def __str__(self):
        return str('{} {} {} {}'.format(self.pay_day, self.name, self.raw_weight, self.raw_price))

    def __lt__(self, other):
        return self.profit > other.profit

    def get_profit(self):
        return self.price - self.weight


def get_optimal_packet(data: List, budget: int, total_days: int) -> List[Bond]:
    bonds = [Bond(name=i[1], weight=i[2], price=i[3], total_days=total_days, pay_day=i[0]) for i in data]
    bonds.sort()
    min_weight = min([i.weight for i in bonds])

    result = []
    for stuff in bonds:
        if budget < min_weight:
            break
        result.append(stuff)
        budget -= stuff.weight

    return result


def get_packet_profit(bonds: List[Bond]) -> int:
    return sum([i.price for i in bonds]) - sum([i.weight for i in bonds])


if __name__ == '__main__':
    data = []
    general_data = input()
    budget = int(general_data.split(' ')[2])
    total_days = int(general_data.split(' ')[1])
    while True:
        line = input()
        if len(line) == 0:
            break

        data_str = line.split(' ')
        data.append([int(data_str[0]), data_str[1], float(data_str[2]), int(data_str[3])])

    optimal_packet = get_optimal_packet(data, budget, total_days)
    profit = get_packet_profit(optimal_packet)
    print(profit)
    for bond in optimal_packet:
        print(bond)
    print()
